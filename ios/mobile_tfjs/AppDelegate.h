#import <React/RCTBridgeDelegate.h>
#import <UIKit/UIKit.h>

// penambahan UMCore
#import <UMCore/UMAppDelegateWrapper.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, RCTBridgeDelegate>

@property (nonatomic, strong) UIWindow *window;

@end
